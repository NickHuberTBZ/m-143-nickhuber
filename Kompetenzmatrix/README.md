## Kompetenzmatrix - Modul 143


## Kompetenzstufen

### Grundlagen | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.0.*

### Fortgeschritten | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.5*

### Erweitert | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*

---

| Kompetenzband: | HZ | Grundlagen | Fortgeschritten | Erweitert |
| --- | --- | --- | --- | --- |
| Datensicherheitskonzept | 1 | A1G: Ich kann die Rahmenbedingungen und Vorschriften, die bei einem Datensicherungssystem zu beachten sind, erklären und wesentliche Einflussfaktoren aufzählen.  | A1F: Ich kann technische und personelle Risiken eines Datensicherungssystems einschätzen und dabei passende Einflussfaktoren bei der Erstellung eines Datensicherungskonzepts berücksichtigen. | A1E: Ich kann vorhandene Datensicherungssysteme analysieren und unter Berücksichtigung von Rahmenbedingungen, Vorschriften und erhobene Daten (Einflussfaktoren) ein Datensicherungskonzept entwickeln und fachgerecht darstellen.
| | 1  | A2G: Ich kann unterschiedliche Datensicherungsverfahren und -technologien erklären und begründen. | A2F: Ich kann Kriterien berücksichtigen, die einen effizienten Einsatz von Backup- und Restore-Systemen ermöglichen. Zudem kann ich das optimale Datensicherungsverfahren für ein Datensicherungskonzept bestimmen. | A2E: Ich kann anhand von aktuellen technischen Lösungen ein optimales Datensicherungsverfahren auswählen. Ich berücksichtige bei der Entwicklung des Datensicherungskonzepts anspruchsvolle Einflussfaktoren und weiss, wie diese bei der Realisierung technisch umgesetzt werden.     
| Machbarkeit | 2 | B1G: Ich kann Kriterien aufzählen, die bei der Machbarkeitsprüfung eines Datensicherungskonzepts berücksichtigt werden sollten. | B1F: Ich kann zwischen technischen und betriebswirtschaftlichen Kriteren unterscheiden und begründen, welche Einflüsse diese auf die Machbarkeit eines Datensicherungskonzepts haben.  | B1E: Ich kann aufgrund von technischen und betriebswirtschaftlichen Kriterien ein individualisiertes Datensicherungskonzept erstellen und auf Machbarkeit überprüfen.   |
| Bedarfsermittlung | 3 | C1G: Ich kann anhand von Vorgaben den Speicherbedarf ermitteln und kenne die Einsatzmerkmale von Backupgeräten und Speichermedien. | C1F: Ich kann anhand von Vorgaben den Speicherbedarf ermitteln und technisch angemessene Lösungen (Geräte, Speichermedien) erheben. | C1E: Ich kann den aktuellen und längerfristigen Speicherbedarf ermitteln und individualisierte Lösungen (Geräte, Speichermedien) sowie optimale Datenstandorte (On-Prem, Cloud) ausarbeiten und anbieten. |
| Sicherungsprozeduren | 4 | D1G: Ich kann verschiedene Befehle, welche für die Erstellung von Sicherungs- und Wiederherstellungsprozeduren notwendig sind, erklären. | D1F: Ich kann ein bestehendes Script, welches für die Sicherungs- und Wiederherstellung von Daten genutzt wird, verstehen und bestimmten Anforderungen anpassen. | D1E: Ich kann selbständig eine funktionsfähige Sicherungs- und Wiederherstellungsprozedur erstellen, automatisieren, testen und individuellen Anforderungen anpassen. |
| | 4 | D2G: Ich kann feststellen, ob eine Sicherungs- und Wiederherstellungsprozedur korrekt funktioniert. | D2F: Ich kann überprüfen, ob eine bestehende Sicherungs- und Wiederherstellungsprozedur funktioniert und bei Bedarf entsprechende Massnahemen einleiten. | D2E: Ich kann eine bestehenden Sicherung- und Wiederherstellungsprozedur kontinuierlich weiterentwickeln und Änderungen angemessen und zeitnah implementieren.|
| | 4 | D3G: Ich kann wesentliche Aspekte einer Sicherungs- und Wiederherstellungsprozedur erklären und dokumentieren. | D3F: Ich kann eine verständliche Dokumentation einer Sicherungs- und Wiederherstellungsprozedur erstellen. | D3E: Ich kann eine vollständige Dokumentation einer Sicherungs- und Wiederherstellungsprozedur erstellen, welche die Konfiguration sowie die Funktionalität dazu nachvollziehbar abbildet.|
| Sicherungs- und Wiederherstellungsprozesse | 5 | E1G: Ich kann Schritte aufzählen, welche bei einer Sicherungs- und Wiederherstellungsprozedur durchgeführt werden. | E1F: Ich kann anhand eines vorgegebenen Szenarios eine Sicherungs- und Wiederherstellungsprozedur durchführen und überprüfen. | E1E: Ich kann Sicherungs- und Wiederherstellungsprozeduren erstellen, prüfen, automatisieren und bei Bedarf optimieren oder anpassen.|
| Betriebs- und Wartungsdokumentation | 6 | E2G: Ich kann begründen, weshalb eine aktuell nachgeführte Betriebs- und Wartungsdokumentation notwendig ist. | E2F: Ich kenne eine Methode, um die Betriebs- und Wartungsdokumentation aktuell zu halten und kann die Anwendung erklären. | E2E: Ich kann einen Prozess definieren, der die regelmässige Aktualisierung der Betriebs- und Wartungsdokumentation sicherstellt. |



