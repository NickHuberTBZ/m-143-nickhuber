## Umsetzung - D2 für HappyPizza

**Szenario:** Überwachung und Alarmierung von fehlgeschlagenen Backups mit AWS Backup für HappyPizza

### Überwachung und Alarmierung von fehlgeschlagenen Backups mit AWS Backup:

HappyPizza hat AWS Backup implementiert und CloudWatch-Alarme konfiguriert, um das IT-Team zu benachrichtigen, wenn ein Backup fehlschlägt.

- Ein CloudWatch-Alarm wird ausgelöst wenn ein Backup fehlschlägt, wird ein Mail Automatisch an das IT-Team geschikt.
- LogFile im S3 Bucket, dies wird durch ein Skript erstellt. Man findet es hier im Ordner Logs.json.

**Vorteile:**

- Durch die Überwachung und Alarmierung von fehlgeschlagenen Backups kann HappyPizza sicherstellen, dass ihre Daten immer geschützt sind und dass Backups reibungslos durchgeführt werden.
- Durch die Automatisierung der Wiederholung von fehlgeschlagenen Backup-Jobs kann das IT-Team Zeit sparen und sich auf andere kritische Aufgaben konzentrieren.

---

Wenn das Backup nicht läuft, wird ein Alarm an die E-Mail des IT-Teams gesendet.

![](HappyPizza/Screenshots/CloudWatchAlarme.png)

---

Hier sieht man, dass die Backups erfolgreich waren:

![](HappyPizza/Screenshots/WiederherstellungsPunkte.png)

---
