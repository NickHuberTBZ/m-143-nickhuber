## Umsetzung des Projekts - A1 für HappyPizza

### Bisheriges Datensicherungskonzept

Das bisherige Datensicherungskonzept von HappyPizza umfasst ein Rezeptbuch welches in der Küche der beliebten Kette aufbewahrt wird. Es wird immer jährlich eine Kopie des Buches in ein Lager gelegt, dass man sieht wie sich das Rezept über die Jahre verändert und für andere Statistiken.

### Neues Datensicherungskonzept

Das neue Datensicherungskonzept für HappyPizza ist darauf ausgerichtet, die Effizienz und Sicherheit der Datensicherung erheblich zu verbessern. Details finden Sie in meinem [Datensicherungskonzept-Dokument](https://gitlab.com/NickHuberTBZ/m-143-nickhuber/-/tree/main/HappyPizza/Datensicherungskonzept), das den Anforderungen gerecht wird.

---

### User-Story zur Datensicherung in unserem Projekt

1. **Datensicherung des geheimen Teigrezepts:**
   - Automatische Sicherung des Teigrezepts in einem festgelegten Zeitintervall.
   - Sicherung des Teigrezepts in einem sicheren Cloud System, in einem AWS Backup von einem S3 Bucket.

2. **Koordination und Zeitplan:**
   - Erstellung eines detaillierten Zeitplans für die automatische Datensicherung des geheimen Teigrezepts.
   - Berücksichtigung von sicherheitsrelevanten Aspekten bei der Festlegung des Zeitplans.

3. **Sicherheitsaspekte:**
   - Implementierung zusätzlicher Sicherheitsmaßnahmen, um sicherzustellen, dass das geheime Teigrezept vor unbefugtem Zugriff geschützt ist.
   - Gewährleistung der Vertraulichkeit und Integrität des geheimen Teigrezepts durch Verschlüsselung und Zugriffsbeschränkungen.
   - Einrichtung von Redundanzmechanismen in der Cloud-Infrastruktur, um sicherzustellen, dass keine Daten verloren gehen.

Mit diesem neuen Datensicherungskonzept wird sichergestellt, dass das geheime Teigrezept von HappyPizza effizient und sicher gesichert wird, um die Geschäftskontinuität und den Schutz dieses wertvollen Firmenvermögens zu gewährleisten.
