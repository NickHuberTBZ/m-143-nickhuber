## Umsetzung - E1 für HappyPizza

### 1. Systemübersicht

Für das neue Cloud System von HappyPizza habe ich diese Services von AWS verwendet. 

**AWS S3 Bucket:** Der S3 Bucket speichert das geheime Teigrezept von HappyPizza.

**AWS Cloudwatch:** Cloudwatch habe dafür aufgesetzt um die Verwaltung von HappyPizza zu vereinfachen.

**AWS Backup:** AWS Backup um das Teigrezept sicher zu halten.

### 2. Backup Lösung

Im Rahmen des HappyPizza-Projekts wurde ein AWS S3 Bucket mit dem Namen ``happypizza-teigrezept`` erstellt, in dem das geheime Teigrezept von HappyPizza sicher gespeichert ist. Zur Datensicherung und Wiederherstellung wird AWS Backup verwendet, um regelmäßige Backups dieses Buckets durchzuführen, wodurch die Integrität des wertvollen Rezepts gewährleistet wird.

### 3. Überprüfung und Alamierung

Es wurden CloudWatch-Alarme eingerichtet, um das IT-Team über fehlgeschlagene Backups zu informieren. Ein CloudWatch-Alarm mit dem Namen "BackupJobFailed" wurde konfiguriert. Dieser Alarm ist so konfiguriert, dass er ausgelöst wird, wenn innerhalb eines Zeitraums von 24 Stunden mehr als 3 Backup-Jobs fehlschlagen.

### 4. Verwaltung


**Verwaltung:** Das schlussendliche Verwalten des Cloud Systems liegt in den Händen des Arbeitsgeber, also dem IT-Team von HappyPizza.
**Fehlerbehbungen:** Bei schwierigen Fällen, kann man auch in Zukunft auf den Arbeitsnehmer zugehen.
