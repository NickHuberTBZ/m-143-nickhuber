## Umsetzung des Projekts - B1

Um den aktuellen und langfristigen Speicherbedarf von HappyPizza zu ermitteln und individuelle Lösungen für die Datenspeicherung zu entwickeln, wurden die folgenden Schritte unternommen:

[TOC]

### Schritt 1: Analyse des Speicherbedarfs

Zunächst wurde eine gründliche Analyse des aktuellen Speicherbedarfs durchgeführt. Dies umfasste die Erfassung der Datenmenge, die regelmäßig für das Teigrezept von HappyPizza benötigt wird. Aufgrund der begrenzten Größe des Dokuments wurde festgestellt, dass ein einfaches Textdokument nur sehr wenig Speicherplatz erfordert.

### Schritt 2: Langfristige Speicherbedarfsprognose

Um den langfristigen Speicherbedarf von HappyPizza zu prognostizieren, wurden verschiedene Faktoren berücksichtigt. Dies schloss zukünftiges Wachstum, erwartete Änderungen am Dokument und potenzielle Erweiterungen ein. Es wurde festgestellt, dass der langfristige Speicherbedarf ebenfalls relativ gering sein wird.

### Schritt 3: Entwicklung individueller Lösungen

Basierend auf der Analyse des Speicherbedarfs wurden individuelle Lösungen entwickelt, um die Datenspeicherung zu optimieren. Angesichts der begrenzten Größe des Dokuments und der Anforderung, physische Schäden zu verhindern, wurde die folgende Lösung gewählt:

- **S3 Bucket:** Das Teigrezept wird in einem S3 Bucket gespeichert, die in der Cloud gehostet wird. Dies bietet eine sichere und skalierbare Speicherlösung.

- **AWS Backup:** Der S3 Bucket kann einfach und simpel durch den AWS Anbieter gespeichert werden, dass eine flexible Speicherlösung in der Cloud darstellt.

- **Regelmäßige Sicherung:** Ein Verbindungsskript sichert die Daten regelmäßig in der Cloud, um sicherzustellen, dass das Teigrezept vor Datenverlust geschützt ist.

Diese individuellen Lösungen gewährleisten die Sicherheit und Verfügbarkeit des Teigrezepts von HappyPizza und erfüllen gleichzeitig die Anforderungen an den Speicherbedarf, ohne unnötige Ressourcen in Anspruch zu nehmen. Sie bieten die notwendige Flexibilität für zukünftiges Wachstum und Erweiterungen.
