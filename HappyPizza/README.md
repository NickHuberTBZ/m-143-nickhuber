# HappyPizza Datensicherungsoptimierung

## Projektbeschreibung

Das Projekt "HappyPizza Datensicherungsoptimierung" zielt darauf ab, die bestehenden Datensicherungssysteme der renommierten Pizzafirma "HappyPizza" zu analysieren, zu verbessern und an die aktuellen Anforderungen anzupassen. Dabei sollen verschiedene Einflussfaktoren, Rahmenbedingungen und Vorschriften berücksichtigt werden, um ein robustes Datensicherungskonzept zu entwickeln und zu implementieren.

Projektphasen und Ziele

**Phase 1: Analyse und Bewertung**

- **A1:** Analyse und Bewertung der bestehenden Datensicherungssysteme von HappyPizza unter Berücksichtigung der geltenden Vorschriften und Rahmenbedingungen.
- **A2:** Untersuchung aktueller technischer Lösungen und Auswahl des optimalen Datensicherungsverfahrens.

**Phase 2: Konzeptentwicklung und Realisierung**

- **B1:** Ermittlung des aktuellen und langfristigen Speicherbedarfs von HappyPizza und Entwicklung individualisierter Lösungen für die Datenspeicherung, einschließlich geeigneter Geräte, Speichermedien und Datenstandorte (On-Premises, Cloud).
- **C1:** Erstellung einer funktionsfähigen Sicherungs- und Wiederherstellungsprozedur, Automatisierung und umfassende Tests, um sicherzustellen, dass sie den individuellen Anforderungen von HappyPizza entspricht.

**Phase 3: Kontinuierliche Verbesserung und Dokumentation**
- **D1:** Ich kann selbständig eine funktionsfähige Sicherungs- und Wiederherstellungsprozedur erstellen, automatisieren, testen und individuellen Anforderungen anpassen.
- **D2:** Implementierung kontinuierlicher Verbesserungen in die Sicherungs- und Wiederherstellungsprozedur, um den sich ändernden Anforderungen gerecht zu werden.
- **D3:** Erstellung einer umfassenden Dokumentation der Sicherungs- und Wiederherstellungsprozedur, die die Konfiguration und Funktionalität nachvollziehbar abbildet.
- **E1:** Regelmäßige Überwachung und Optimierung der Sicherungs- und Wiederherstellungsprozeduren von HappyPizza.
- **E2:** Definition eines Prozesses zur regelmäßigen Aktualisierung der Betriebs- und Wartungsdokumentation.

## Projektabschluss

Nach Abschluss des Projekts wird HappyPizza über ein effizientes, individualisiertes Datensicherungssystem verfügen, das den aktuellen Best Practices und Vorschriften entspricht und gleichzeitig die Flexibilität bietet, sich den zukünftigen Anforderungen anzupassen. Eine umfassende Dokumentation wird sicherstellen, dass das System gut gewartet und weiterentwickelt werden kann, um die Datensicherheit der Pizzafirma langfristig zu gewährleisten.

## Aufteilung der Umsetzung

Die Umsetzung des Projekts "HappyPizza Datensicherungsoptimierung" ist in verschiedene Teile unterteilt, die in diesem Dokument zu finden sind. Jeder Teil entspricht einem Abschnitt in der Kompetenzmatrix. In den folgenden Links finden Sie das jeweilige File und die verschiedenen Teile der Kompetenzmatrix sowie die Umsetzung davon.

- [Kompetenzmatrix](https://gitlab.com/NickHuberTBZ/m-143-nickhuber/-/blob/main/Kompetenzmatrix)


Für weitere Details und Informationen zu der Kompetenzmatrix, klicken sie auf denn Link oben.

