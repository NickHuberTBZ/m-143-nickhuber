# Datensicherungskonzept für die Firma HappyPizza

**Zweck dieses Dokuments:**

Dieses Dokument enthält das Datensicherungskonzept für die Firma HappyPizza. Ziel ist es, das geheime Teigrezept der Firma sicher und zuverlässig zu sichern und automatisch in einem **AWS Backup** von einer **EC2 Instanz** gespeichert.

**EC2** - AWS EC2 ermöglicht das schnelle Bereitstellen von virtuellen Servern in der Cloud

**AWS Backup** - Während AWS Backup ein Service zur zentralen Verwaltung und Automatisierung von Sicherungen in der AWS-Umgebung ist.


## Verantwortliche Personen
- Die IT-Abteilung von HappyPizza ist für die Sicherung des Teigrezepts verantwortlich. Der Hauptverantwortliche ist Herr Peter Meier.

## IT-Systeme
- Die zu sichernden Daten, das geheime Teigrezept, befinden sich auf einer EC2 in der AWS Cloud.

## Datenspeicherarten und Formate
- Das Teigrezept wird auf der EC2-Instanz gespeichert und anschließend mithilfe eines automatisierten Prozesses mit AWS Backup abgespeichert

## Art der Datensicherung/Häufigkeit der Sicherungen
- Es wird eine automatisierte Datensicherung verwendet. Das Teigrezept wird täglich von der EC2 mit einem AWS Backup gesichert, um sicherzustellen, dass keine Daten verloren gehen und einmal am Ende vom Jahr damit man die jährliche Veränderung des Rezeptes sieht

## Cloud-Speicher
- Die Sicherungskopien des Teigrezepts werden sicher in dem AWS Backup gespeichert.

## Prozesse zum Zurückspielen/Wiederherstellen von Daten
- Im Falle eines Datenverlusts oder Bedarfs an Wiederherstellung kann das Teigrezept einfach aus dem AWS Backup auf die EC2 zurückgespielt werden.

## Vorgaben zur Nutzung der Daten für andere Zwecke
- Die gesicherten Daten dürfen nur zur Wiederherstellung des Teigrezepts verwendet werden und unterliegen strengen Datenschutzrichtlinien. Jegliche Nutzung für andere Zwecke ist nicht gestattet.

Dieses Datensicherungskonzept gewährleistet, dass das geheime Teigrezept von HappyPizza sicher und zuverlässig gesichert wird, um den kontinuierlichen Betrieb und den Schutz dieses wertvollen Unternehmensgeheimnisses zu gewährleisten.
