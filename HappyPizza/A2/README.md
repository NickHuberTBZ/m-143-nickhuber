## Umsetzung des Projekts - A2E für HappyPizza

### Untersuchung aktueller technischer Lösungen und Auswahl des optimalen Datensicherungsverfahrens

Um die Datensicherung für HappyPizza zu optimieren, wurde eine gründliche Untersuchung aktueller technischer Lösungen durchgeführt, um das optimale Datensicherungsverfahren auszuwählen.

### Anforderungsanalyse für HappyPizza

Bei der Anforderungsanalyse für HappyPizza wurde schnell deutlich, dass das Hauptziel darin besteht, das geheime Teigrezept zu sichern. Dieses Rezept ist nicht besonders groß und benötigt daher keinen komplexen und ressourcenintensiven Ansatz. Die primären Anforderungen waren:

- **Effizienz**: Da das Teigrezept nicht viel Speicherplatz beansprucht, muss die Lösung ressourceneffizient sein.

- **Einfachheit**: Die Lösung sollte einfach und unkompliziert sein, um die Verwaltung zu erleichtern.

- **Kosteneffektivität**: Da die Datenmenge gering ist, sollte die Lösung kostengünstig sein.

### Resultat meiner Analyse und Lösung

In Anbetracht der Tatsache, dass es sich bei den zu sichernden Daten von HappyPizza nur um ein kurzes Textdokument handelt, wird schnell klar, dass ein großes und komplexes Datensicherungssystem nicht erforderlich ist. Allerdings wurde die Vorgabe gemacht, dass dieses Dokument in einer Cloud gesichert werden sollte, um es vor physischen Schäden zu schützen, wie sie bei Bränden auftreten können. Diese Anforderung wurde durch die bisherigen Erfahrungen mit dem Verlust von Rezeptbüchern nach Bränden begründet.

Die gewählte Lösung besteht darin, das Textdokument in einem S3 Bucket zu speichern und danach mit Hilfe von AWS Backup immer gespeichert wird.

 ![Datensicherungsverfahren](Datensicherungsverfahren.png)
 
Diese Lösung gewährleistet die Datensicherheit und Schutz vor physischen Schäden und ermöglicht gleichzeitig den bequemen Zugriff auf AWS bietet das Teigrezept von jedem Ort aus. n die notwendige Flexibilität und Sicherheit, um die Anforderungen von HappyPizza effizient zu erfüllen und das wertvolle Rezept vor Verlust zu schützen.
